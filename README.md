# BEXS Challenge

## What you can find here?

This repository contains a solution for the problem [Rota de Viagem](challenge.md).

It uses Docker to setup the environment, so you don't need to install on your system the Python project dependencies! But, if you prefer, the `pipenv` files ([`Pipfile`](Pipfile) and [`Pipfile.lock`](Pipfile.lock)) is also available so you can install everything on your computer!

## How to run?

There's an initial configuration already setup that you can find [here](resources/input-file.txt), so if you only want to see the app running, just type on your favorite shell:

- If you have `docker-compose`:

```bash
docker-compose run -p 8000:8000 app
```

- If you don't have Docker, be sure that you have at least Python 3.8:

```bash
./main.py resources/input-file.txt
```

This will spin up the REST interface and the CLI, so you can start interacting with the app!

## And the tests?

Wanna run the tests? Just type:

- With `docker-compose`:

```bash
docker-compose run tests
```

- If you don't have Docker, install the `dev-packages` from [`Pipfile`](Pipfile) and run:

```bash
pipenv run tox
```

## App structure

The main entrance for the app is the file [`main.py`](main.py) which starts an CLI and a [Uvicorn](http://www.uvicorn.org/) written with the [FastAPI](https://fastapi.tiangolo.com/) framework.

Besides that, you can find all the source code under the folder `bexs_challenge`. A little explanation for each file:

- `exceptions.py`: Custom exceptions used throughout the code
- `models.py`: The models used across the code, such as how the route network was structured, etc
- `network.py`: The heart of the solution. It contains the algorithm and any related method for the network
- `settings.py`: A central module that holds the app configuration
- `interfaces/cli.py`: The CLI app interface (input, output, etc)
- `interfaces/rest.py`: The REST app interface (routes, FastAPI app, etc)


## The solution

The solution for the problem uses a Dijsktra algorithm to find the cheapest and shortest route between given points! It uses a priority queue and the graph is represented as a map which the key is the nodes, or our airports, and its value is another map which the key is the neighbour and the value is a list of edge cost. To exemplify, suppose we have a route between CDG and GRU that costs $30. Then, our graph will be represented as:

```json
{
    "CDG": {
        "GRU": [30]
    },
    "GRU": {}
}
```

It was chosen like that so that we have a easy access to the nodes and edges.

Another important thing to mention is that when a new route is added through the REST interface, the file is updated on a background task so that the request doesn't block.

## The REST interface

|Method|Endpoint|Description|URI example|
|------|--------|-----------|-----------|
| POST | /routes/ | Inserts a new route. You can find the expected body [here](#markdown-header-new-route-json) | - |
| GET  | /best-route/ | Retrieve the best route! It expects the queries `source` and `destination` and the expected JSON body returned can be found [here](#markdown-header-best-route-json) | /best-route/?source=GRU&destination=CDG |

### New route JSON

```json
{
    "source": "string",
    "destination": "string",
    "cost": "integer"
}
```

Example:

```json
{
    "source": "GRU",
    "destination": "CGD",
    "cost": 42
}
```

### Best route JSON

```json
{
    "best_route": ["string"],
    "cost": "integer"
}
```

Example:

```json
{
    "best_route": ["GRU","BRC","SCL","ORL","CDG"],
    "cost": 40
}
```