#!/usr/bin/env python
import argparse
from threading import Thread

import uvicorn

from bexs_challenge.interfaces import cli
from bexs_challenge.interfaces import rest
from bexs_challenge.settings import HOST
from bexs_challenge.settings import PORT
from bexs_challenge.settings import UVICORN_LOG_LEVEL

parser = argparse.ArgumentParser(description="Find the cheapest route from A to B!")
parser.add_argument(
    "file_path",
    metavar="csv_file_path",
    type=str,
    help="A path to a CSV file with initial routes network configuration",
)


if __name__ == "__main__":
    args = parser.parse_args()
    network = cli.create_initial_network(args.file_path)
    rest.set_configuration(args.file_path, network)
    cli_thread = Thread(
        target=lambda: cli.main(network),
        daemon=True,
    ).start()
    uvicorn.run(rest.app, host=HOST, port=PORT, log_level=UVICORN_LOG_LEVEL.lower())
