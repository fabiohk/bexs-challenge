import os

import pytest

from bexs_challenge.exceptions import InputWithError
from bexs_challenge.interfaces import cli


def test_should_succesfully_create_a_network():
    file_path = os.path.join(os.path.dirname(__file__), os.path.pardir, "resources", "cli_example.csv")

    network = cli.create_initial_network(file_path)

    assert network == {"GRU": {"BRC": [10]}, "BRC": {}}


def test_should_correctly_parse_an_input():
    network = {"GRU": {"BRC": [10]}, "BRC": {}}

    source, destination = cli.parse_input(network, "GRU-BRC")

    assert source == "GRU"
    assert destination == "BRC"


def test_should_raise_input_error_exception_when_couldnt_understand_input():
    network = {"GRU": {"BRC": [10]}, "BRC": {}}

    with pytest.raises(InputWithError):
        cli.parse_input(network, "GRU")
