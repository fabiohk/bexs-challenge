import csv
import os

import pytest
from fastapi.testclient import TestClient

from bexs_challenge.interfaces import rest
from bexs_challenge.models import RouteNetwork


@pytest.fixture
def route_network() -> RouteNetwork:
    return {"GRU": {"BRC": [10]}, "BRC": {}}


@pytest.fixture
def file_path() -> str:
    return os.path.join(os.path.dirname(__file__), os.path.pardir, "resources", "rest_example.csv")


@pytest.fixture
def app_client(route_network: RouteNetwork, file_path: str) -> TestClient:
    rest.set_configuration(file_path, route_network)
    return TestClient(rest.app)


def test_should_set_route_network_and_file_path_as_attribute_of_app(route_network: RouteNetwork):
    rest.set_configuration("file_path", route_network)

    assert getattr(rest.app, "network_file_path") == "file_path"
    assert getattr(rest.app, "route_network") == route_network


def test_should_add_new_route(app_client: TestClient, file_path: str):
    app_client = TestClient(rest.app)

    response = app_client.post("/routes/", json={"source": "BRC", "destination": "GRU", "cost": 42})

    assert response.status_code == 201
    assert response.json() == {"source": "BRC", "destination": "GRU", "cost": 42}

    assert rest.app.route_network == {"GRU": {"BRC": [10]}, "BRC": {"GRU": [42]}}

    with open(file_path, "r") as file:
        lines = file.read().splitlines()
        last_line = lines[-1]

        assert last_line == "BRC,GRU,42"


def test_shouldnt_add_new_route_when_cost_is_invalid(app_client: TestClient):
    response = app_client.post("/routes/", json={"source": "BRC", "destination": "GRU", "cost": "asd"})

    assert response.status_code == 422
    assert response.json() == {
        "detail": [{"loc": ["body", "cost"], "msg": "value is not a valid integer", "type": "type_error.integer"}]
    }

    assert rest.app.route_network == {"GRU": {"BRC": [10]}, "BRC": {}}  # Still the same


@pytest.mark.parametrize("field", ["source", "destination", "cost"])
def test_shouldnt_add_new_route_when_required_field_is_missing(field: str, app_client: TestClient):
    body = {"source": "BRC", "destination": "GRU", "cost": 42}
    body.pop(field)

    response = app_client.post("/routes/", json=body)

    assert response.status_code == 422
    assert response.json() == {
        "detail": [{"loc": ["body", field], "msg": "field required", "type": "value_error.missing"}]
    }

    assert rest.app.route_network == {"GRU": {"BRC": [10]}, "BRC": {}}  # Still the same


def test_should_retrieve_best_route(app_client: TestClient):
    response = app_client.get("/best-route/", params={"source": "GRU", "destination": "BRC"})

    assert response.status_code == 200
    assert response.json() == {"best_route": ["GRU", "BRC"], "cost": 10}


@pytest.mark.parametrize("field", ["source", "destination"])
def test_shouldnt_retrieve_best_route_when_required_field_is_missing(field: str, app_client: TestClient):
    params = {"source": "GRU", "destination": "BRC"}
    params.pop(field)

    response = app_client.get("/best-route/", params=params)
    assert response.status_code == 422
    assert response.json() == {
        "detail": [{"loc": ["query", field], "msg": "field required", "type": "value_error.missing"}]
    }


def test_should_return_couldnt_find_best_route_when_it_is_not_possible(app_client: TestClient):
    params = {"source": "BRC", "destination": "GRU"}

    response = app_client.get("/best-route/", params=params)
    assert response.status_code == 400
    assert response.json() == {"detail": "Couldn't find a route between the given points!"}


def test_should_return_couldnt_find_best_route_given_an_unknown_airport_as_source(app_client: TestClient):
    params = {"source": "ABC", "destination": "BRC"}

    response = app_client.get("/best-route/", params=params)
    assert response.status_code == 400
    assert response.json() == {"detail": "Cannot find a route for an unknown airport!"}


def test_should_return_couldnt_find_best_route_given_an_unknown_airport_as_destination(app_client: TestClient):
    params = {"source": "GRU", "destination": "ABC"}

    response = app_client.get("/best-route/", params=params)
    assert response.status_code == 400
    assert response.json() == {"detail": "Cannot find a route for an unknown airport!"}
