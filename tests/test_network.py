import pytest

from bexs_challenge.exceptions import UnknownAirport
from bexs_challenge.network import add_new_route
from bexs_challenge.network import retrieve_cheaper_route


def test_should_add_a_completely_new_route():
    network = {}  # Completely empty route network
    source, destination, cost = "GRU", "BRC", 10

    network = add_new_route(network, source, destination, cost)

    assert network == {source: {destination: [cost]}, destination: {}}


def test_should_add_a_new_cost_to_an_existing_route():
    source, destination, first_cost, another_cost = "GRU", "BRC", 10, 20
    network = {"GRU": {"BRC": [first_cost]}}

    network = add_new_route(network, source, destination, another_cost)

    assert network == {source: {destination: [first_cost, another_cost]}, destination: {}}


def test_should_a_new_route_for_an_existing_airport():
    source, destination, cost, another_destination = "GRU", "BRC", 10, "SCL"
    network = {"GRU": {"BRC": [cost]}}

    network = add_new_route(network, source, another_destination, cost)

    assert network == {source: {destination: [cost], another_destination: [cost]}, another_destination: {}}


def test_should_find_the_cheaper_route_from_a_simple_network():
    network = {"GRU": {"BRC": [10]}, "BRC": {}}

    cheaper_route, cost = retrieve_cheaper_route(network, "GRU", "BRC")

    assert cheaper_route == ["GRU", "BRC"]
    assert cost == 10


def test_should_find_the_cheaper_route_from_a_simple_network_with_route_with_multiple_costs():
    network = {"GRU": {"BRC": [10, 99, 42]}, "BRC": {}}

    cheaper_route, cost = retrieve_cheaper_route(network, "GRU", "BRC")

    assert cheaper_route == ["GRU", "BRC"]
    assert cost == 10


def test_should_find_the_cheaper_route_from_a_network():
    network = {
        "GRU": {"BRC": [10], "CDG": [75], "SCL": [20], "ORL": [56]},
        "BRC": {"SCL": [5]},
        "ORL": {"CDG": [5]},
        "SCL": {"ORL": [20]},
        "CDG": {},
    }

    cheaper_route, cost = retrieve_cheaper_route(network, "GRU", "CDG")

    assert cheaper_route == ["GRU", "BRC", "SCL", "ORL", "CDG"]
    assert cost == 40


def test_should_find_the_shortest_path_with_the_cheaper_route():
    network = {
        "GRU": {"BRC": [1], "CDG": [10]},
        "BRC": {"GRU": [1], "ORL": [4]},
        "ORL": {"BRC": [4], "SCL": [7]},
        "SCL": {"ORL": [7], "CDG": [2]},
        "CDG": {"GRU": [10], "SCL": [2]},
    }

    cheaper_route, cost = retrieve_cheaper_route(network, "GRU", "SCL")

    assert cheaper_route == ["GRU", "CDG", "SCL"]
    assert cost == 12


def test_should_raise_exception_when_source_is_unknown():
    network, unknown_source, destination = (
        {
            "GRU": {"BRC": [1]},
            "BRC": {"GRU": [1]},
        },
        "ABC",
        "GRU",
    )

    with pytest.raises(UnknownAirport):
        retrieve_cheaper_route(network, unknown_source, destination)


def test_should_raise_exception_when_destination_is_unknown():
    network, source, unknown_destination = (
        {
            "GRU": {"BRC": [1]},
            "BRC": {"GRU": [1]},
        },
        "GRU",
        "ABC",
    )

    with pytest.raises(UnknownAirport):
        retrieve_cheaper_route(network, source, unknown_destination)
