import logging
import logging.config
import os

HOST = os.getenv("HOST", "0.0.0.0")
PORT = int(os.getenv("PORT", 8000))
ROOT_LOG_LEVEL = os.getenv("LOG_LEVEL", "WARNING")
UVICORN_LOG_LEVEL = os.getenv("UVICORN_LOG_LEVEL", "WARNING")


logging.config.dictConfig(
    {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "standard": {
                "()": logging.Formatter,
                "format": "%(levelname)-8s [%(asctime)s] %(name)s: %(message)s",
            }
        },
        "handlers": {"console": {"class": "logging.StreamHandler", "formatter": "standard"}},
        "loggers": {
            "": {"level": ROOT_LOG_LEVEL, "handlers": ["console"]},
            "uvicorn": {"level": UVICORN_LOG_LEVEL, "handlers": ["console"], "propagate": False},
        },
    }
)
