from typing import Dict
from typing import List
from typing import NewType

Airport = NewType("Airport", str)
RouteCost = NewType("RouteCost", int)
RouteDestinations = Dict[Airport, List[RouteCost]]
RouteNetwork = Dict[Airport, RouteDestinations]
