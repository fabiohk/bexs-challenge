from typing import Any
from typing import Dict
from typing import Optional

from fastapi.exceptions import HTTPException


class InputWithError(Exception):
    pass


class UnknownAirport(HTTPException):
    def __init__(
        self,
        status_code: int = 400,
        detail: str = "Cannot find a route for an unknown airport!",
        headers: Optional[Dict[str, Any]] = None,
    ):
        super().__init__(status_code, detail, headers)


class NoRouteForDestination(HTTPException):
    def __init__(
        self,
        status_code: int = 400,
        detail: str = "Couldn't find a route between the given points!",
        headers: Optional[Dict[str, Any]] = None,
    ):
        super().__init__(status_code, detail, headers)
