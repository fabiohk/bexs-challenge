import heapq
import logging
from enum import auto
from typing import List

from bexs_challenge.exceptions import NoRouteForDestination
from bexs_challenge.exceptions import UnknownAirport
from bexs_challenge.models import Airport
from bexs_challenge.models import RouteNetwork

logger = logging.getLogger(__name__)

UNDEFINED = auto()


def add_new_route(network: RouteNetwork, source: Airport, destination: Airport, cost: int) -> RouteNetwork:
    current_destinations = network.setdefault(source, {})
    costs = current_destinations.setdefault(destination, [])
    costs.append(cost)
    network.setdefault(destination, {})
    logger.debug("Succesfully added a new route! Resulting route network: %s", network)
    return network


class AlgorithmHelperContainer:
    """
    A helper container class used in the Dijkstra's algorithm. It stores three dictionary where the keys are
    the network nodes (airports) and the values are the minimum cost from source, previous airport on the best
    path and the routes taken from source for the airport key.

    Used only so we can have a more clear code...
    """

    def __init__(self, network: RouteNetwork, source: Airport):
        self.min_costs = {airport: float("inf") if airport != source else 0 for airport in network}
        self.previous = {airport: UNDEFINED for airport in network}
        self.routes_taken = {airport: 0 for airport in network}
        self.source = source

    def min_cost_from_source_to(self, airport: Airport) -> float:
        return self.min_costs[airport]

    def set_min_cost_for(self, airport: Airport, new_cost: float):
        self.min_costs[airport] = new_cost

    def previous_airport_of(self, airport: Airport) -> Airport:
        return self.previous[airport]

    def set_previous_airport_for(self, airport: Airport, previous_airport: Airport):
        self.previous[airport] = previous_airport

    def routes_taken_from_source_to(self, airport: Airport) -> int:
        routes_taken = self.routes_taken.get(airport, 0)
        logger.debug("Routes taken for %s from %s: %s", airport, self.source, routes_taken)
        return routes_taken

    def set_routes_taken_from_source_for(self, airport: Airport, routes_taken: int):
        self.routes_taken[airport] = routes_taken

    def is_eligible_to_change_route(
        self,
        cost_to_neighbour_from_source: float,
        airport: Airport,
        neighbour_airport: Airport,
    ) -> bool:
        return cost_to_neighbour_from_source < self.min_cost_from_source_to(neighbour_airport) or (
            cost_to_neighbour_from_source == self.min_cost_from_source_to(neighbour_airport)
            and self.routes_taken_from_source_to(airport) + 1 < self.routes_taken_from_source_to(neighbour_airport)
        )

    def change_route_for_airport(self, airport: Airport, previous_airport: Airport, new_cost: float, routes_taken: int):
        self.set_min_cost_for(airport, new_cost)
        self.set_previous_airport_for(airport, previous_airport)
        self.set_routes_taken_from_source_for(airport, routes_taken)

    def retrieve_path_to(self, destination: Airport) -> List[Airport]:
        inverted_path, current_airport = [], destination

        while current_airport != self.source and current_airport != UNDEFINED:
            inverted_path.append(current_airport)
            current_airport = self.previous_airport_of(current_airport)

        if current_airport == UNDEFINED:
            raise NoRouteForDestination

        return [self.source] + inverted_path[::-1]


def retrieve_cheaper_route(network: RouteNetwork, source: Airport, destination: Airport):
    """
    Retrieve the cheaper path from route A to B using the Dijsktra's algorithm for the
    shortest path problem.

    The problem is modelled so that we have the airport as the nodes, the routes is the edges where
    the cost is the weight of each route.

    If there's any route with more than one option (more than one cost), we consider for the calculation
    only the route with the cheaper cost. Why? Well, suppose there's a route with costs k and l, where k < l,
    also, suppose that the cheaper path from A-B uses that route with cost l. But, since k < l, we actually
    have a cheaper path that uses the route with cost k! In conclusion, the cheaper path must always use
    the cheaper routes.
    """
    if source not in network or destination not in network:
        raise UnknownAirport

    helper = AlgorithmHelperContainer(network, source)
    priority_queue = []

    heapq.heappush(priority_queue, (0, source))

    while priority_queue:
        logger.debug("Current priority queue: %s", priority_queue)
        route_cost, airport = heapq.heappop(priority_queue)
        for neighbour_airport, route_costs in network[airport].items():
            cost_to_neighbour_from_source = helper.min_cost_from_source_to(airport) + min(route_costs)
            logger.debug(
                "Cost to go to airport %s from %s passing through %s: %s",
                neighbour_airport,
                source,
                airport,
                cost_to_neighbour_from_source,
            )
            should_change_route = helper.is_eligible_to_change_route(
                cost_to_neighbour_from_source,
                airport,
                neighbour_airport,
            )
            logger.debug("Should change route? %s", should_change_route)

            if should_change_route:
                routes_taken = helper.routes_taken_from_source_to(airport) + 1
                logger.debug("Routes taken: %s", routes_taken)
                helper.change_route_for_airport(neighbour_airport, airport, cost_to_neighbour_from_source, routes_taken)
                heapq.heappush(priority_queue, (min(route_costs), neighbour_airport))

    logger.debug("Final routes taken: %s", helper.routes_taken)

    cheaper_path = helper.retrieve_path_to(destination)

    return cheaper_path, helper.min_cost_from_source_to(destination)
