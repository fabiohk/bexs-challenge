import csv
import logging

from fastapi import BackgroundTasks
from fastapi import FastAPI
from pydantic import BaseModel

from bexs_challenge.models import Airport
from bexs_challenge.models import RouteCost
from bexs_challenge.models import RouteNetwork
from bexs_challenge.network import add_new_route
from bexs_challenge.network import retrieve_cheaper_route

logger = logging.getLogger(__name__)


class Route(BaseModel):
    source: Airport
    destination: Airport
    cost: RouteCost


app = FastAPI()


def set_configuration(file_path: str, network: RouteNetwork):
    setattr(app, "network_file_path", file_path)
    setattr(app, "route_network", network)


def write_new_route_to_file(file_path: str, source: Airport, destination: Airport, cost: RouteCost):
    with open(file_path, "a") as file:
        file.write("\n")
        csv_writer = csv.writer(file)
        csv_writer.writerow([source, destination, cost])


@app.post("/routes/", status_code=201)
def add_route(new_route: Route, background_tasks: BackgroundTasks):
    logger.info("Received request to add a new route; %s", new_route)
    add_new_route(app.route_network, new_route.source, new_route.destination, new_route.cost)
    background_tasks.add_task(
        write_new_route_to_file, app.network_file_path, new_route.source, new_route.destination, new_route.cost
    )
    return new_route


@app.get("/best-route/")
def retrieve_best_route(source: str, destination: str):
    logger.info("Received a request to retrieve the cheapest route from %s to %s", source, destination)
    cheaper_route, cost = retrieve_cheaper_route(app.route_network, source, destination)
    return {"best_route": cheaper_route, "cost": cost}
