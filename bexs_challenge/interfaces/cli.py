import csv
import logging
from collections import namedtuple
from contextlib import suppress
from typing import Tuple

from bexs_challenge.exceptions import InputWithError
from bexs_challenge.exceptions import NoRouteForDestination
from bexs_challenge.exceptions import UnknownAirport
from bexs_challenge.models import Airport
from bexs_challenge.models import RouteNetwork
from bexs_challenge.network import add_new_route
from bexs_challenge.network import retrieve_cheaper_route

logger = logging.getLogger(__name__)

BestPath = namedtuple("BestRoute", ["path", "cost"])


def read_input() -> str:
    print("please enter the route: ", end="")
    return input()


def parse_input(network: RouteNetwork, line_inputted: str) -> Tuple[Airport, Airport]:
    try:
        source, destination = line_inputted.split("-")
    except ValueError:
        logger.info("Couldn't understand the given input...")
        raise InputWithError

    return source, destination


def create_initial_network(file_path: str) -> RouteNetwork:
    network: RouteNetwork = {}

    with open(file_path, "r") as file:
        csv_reader = csv.reader(file)
        for source, destination, cost in csv_reader:
            add_new_route(network, source, destination, int(cost))

    return network


def find_best_path(network: RouteNetwork, source: Airport, destination: Airport) -> BestPath:
    try:
        cheaper_route, cost = retrieve_cheaper_route(network, source, destination)
        path = " - ".join(cheaper_route)
        return BestPath(path, cost)
    except NoRouteForDestination:
        print(f"couldn't find a route from {source} to {destination}")
        raise
    except UnknownAirport:
        print("can't find a route for unknown airports...")
        raise


def main(network: RouteNetwork):
    while True:
        with suppress(InputWithError, UnknownAirport, NoRouteForDestination):
            line_inputted = read_input()
            source, destination = parse_input(network, line_inputted)
            best_path = find_best_path(network, source, destination)
            print(f"best route: {best_path.path} > ${int(best_path.cost)}")
